import model.Circle;
import model.ResizelableCircle;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle = new Circle(5.0);
        ResizelableCircle resizableCircle = new ResizelableCircle(7.0);

        System.out.println(circle.toString());
        System.out.println("Area: " + circle.getArea());
        System.out.println("Perimeter: " + circle.getPerimeter());

        System.out.println(resizableCircle.toString());
        System.out.println("Area: " + resizableCircle.getArea());
        System.out.println("Perimeter: " + resizableCircle.getPerimeter());

        resizableCircle.resize(50);

        System.out.println(resizableCircle.toString());
        System.out.println("Area: " + resizableCircle.getArea());
        System.out.println("Perimeter: " + resizableCircle.getPerimeter());
    }
}
