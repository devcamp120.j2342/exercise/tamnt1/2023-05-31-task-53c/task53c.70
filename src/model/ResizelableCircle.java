﻿package model;

public class ResizelableCircle extends Circle implements Resizeable {

    public ResizelableCircle(double radius) {
        super(radius);

    }

    @Override
    public void resize(int percent) {
        double newRadius = this.radius * (1 + percent / 100.0);
        setRadius(newRadius);
    }

    @Override
    public String toString() {
        return "ResizableCircle [Circle [radius=" + super.toString() + "]]";
    }

}
